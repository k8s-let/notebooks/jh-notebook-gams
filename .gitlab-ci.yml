# This is a GitLab CI configuration to build the project as a docker image
# The file is generic enough to be dropped in a project containing a working Dockerfile
# Author: Florent CHAUVEAU <florent.chauveau@gmail.com>
# Mentioned here: https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

# do not use "latest" here, if you want this to work in the future
image: docker:20.10.14-dind

stages:
  - build
  - push

# Use this if your GitLab runner does not use socket binding
services:
  - name: docker:20.10.14-dind
    command: ["--tls=false"]

variables:
  DOCKER_HOST: tcp://localhost:2375
  DOCKER_TLS_CERTDIR: ""

.docker-script-before: &docker-script-before
  # docker login asks for the password to be passed through stdin for security
  # we use $CI_JOB_TOKEN here which is a special token provided by GitLab
  - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY


Build tag:
  # Build for a tag: write the tag, so the app can read and display the version
  before_script:
    - *docker-script-before
  stage: build
  tags:
    - k8s-runner
  only:
    - tags
  script:
    - echo Building version $CI_COMMIT_TAG
    # fetches the latest image (not failing if image is not found)
    - docker pull $CI_REGISTRY_IMAGE:main || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to
    # the GitLab registry
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg GAMS_LICENSE="$GAMS_LICENSE"
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --build-arg VCS_TAG=$CI_COMMIT_TAG
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

Build docker image:
  before_script:
    - *docker-script-before
  stage: build
  tags:
    - k8s-runner
  except:
    - tags
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to
    # the GitLab registry
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg GAMS_LICENSE="$GAMS_LICENSE"
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --build-arg VCS_TAG=$CI_COMMIT_SHORT_SHA
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

# Here, the goal is to tag the "main" branch as "latest"
Push latest image:
  before_script:
    - *docker-script-before

  needs:
    - Build docker image
  variables:
    # We are just playing with Docker here.
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  only:
    # Only "main" should be tagged "latest"
    - main
  script:
    # Because we have no guarantee that this job will be picked up by the same runner
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    # And we push it.
    - docker push $CI_REGISTRY_IMAGE:latest

# Every other branch
Push branch image:
  before_script:
    - *docker-script-before
  needs:
    - Build docker image
  variables:
    # We are just playing with Docker here.
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  except:
    - main
    - tags
  script:
    # Because we have no guarantee that this job will be picked up by the same runner
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it with the branch name
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    # And we push it.
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME


# Finally, the goal here is to Docker tag any Git tag
# GitLab will start a new pipeline everytime a Git tag is created, which is pretty awesome
Push tag image:
  before_script:
    - *docker-script-before
  needs:
    - Build tag
  variables:
    # Again, we do not need the source code here. Just playing with Docker.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  only:
    # We want this job to be run on tags only.
    - tags
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

