FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal:3.0.0-12

ARG GAMS_LICENSE

USER 0

RUN curl -sSL "https://d37drm4t2jghv5.cloudfront.net/distributions/41.5.0/linux/linux_x64_64_sfx.exe" --create-dirs -o /opt/gams/gams.exe

# Install GAMS 
RUN cd /opt/gams &&\
    chmod +x gams.exe; sync &&\
    ./gams.exe -q &&\
    rm -rf gams.exe && \
    echo $GAMS_LICENSE > /opt/gams/gams41.5_linux_x64_64_sfx/gamslice.txt

# Setup GAMS and Jupyter integration
RUN GAMS_PATH=$(dirname $(find / -name gams -type f -executable -print)) &&\
    ln -s $GAMS_PATH/gams /usr/local/bin/gams &&\
    cd $GAMS_PATH &&\
    ./gamsinst -a -license /opt/gams/gams41.5_linux_x64_64_sfx/gamslice.txt &&\
    chmod -R a+rx /opt/gams && \
    cd /opt/gams/gams41.5_linux_x64_64_sfx/apifiles/Python/api_310 && SETUPTOOLS_USE_DISTUTILS=stdlib python setup.py build -b /tmp/build install

USER 1000
